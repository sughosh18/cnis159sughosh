#include <stdio.h>
int main()
{
  int i, first, last, middle, n, search, array[30];

  printf("Enter the number of elements in an array \n");
  scanf("%d", &n);
  printf("Enter %d elements into the array\n", n);
  for (i = 0; i < n; i++)
  scanf("%d", &array[i]);

  printf("Enter value to find\n");
  scanf("%d", &search);

  first = 0;
  last = n - 1;
  middle = (first+last)/2;

  while (first <= last)
  {
    if (array[middle] < search)
      first = middle + 1;
    else if (array[middle] == search) 
    {
      printf("the element %d is found at location %d.\n", search, middle+1);
      break;
    }
    else
      last = middle - 1;

    middle = (first + last)/2;
  }
  if (first > last)
    printf("The is element %d is Not found! in the array\n", search);

  return 0;
}