#include<stdio.h>
#include<math.h>
struct points_storage
{
    float x,y ;  
} ;
typedef struct points_storage Point;
void input(Point *p1, Point *p2)
{
    printf("Enter two points of coordinates x1,y1 and x2,y2\n");
    scanf("%f%f%f%f",&p1->x,&p1->y,&p2->x,&p2->y);

}   
void compute(Point p1,Point p2,float *distance)
{
    *distance=sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y)) ;

}
void display(Point p1, Point p2, float distance)
{
    printf("The distance between %f,%f and %f,%f is %f",p1.x,p1.y,p2.x,p2.y,distance);

}
int main()
{

    float dist;
    Point p1,p2;
    input(&p1,&p2);
    compute(p1,p2,&dist);
    display(p1,p2,dist);
    return 0;

}