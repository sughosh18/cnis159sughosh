#include<stdio.h>
void input( int *hours , int *minutes)
{
	printf("Enter time in hours \n");
	scanf("%d",hours) ;
	printf("Enter time in Minutes \n") ;
	scanf("%d",minutes) ;
}
void compute (int hours , int minutes , int *result) 
{
	*result = (hours*60)+minutes ;
}
void display(int hours , int minutes , int result)
{
	printf("Time in Hours and Minutes = %d:%d \n Time in Minutes = %d \n",hours,minutes,result) ;
}
int main() 
{
	int h,m,final ;
	input(&h,&m) ;
	compute(h,m,&final);
	display(h,m,final) ;
	return 0 ;
}