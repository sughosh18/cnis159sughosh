#include<stdio.h>
void input(int *num)
{
   printf("Enter a number\n") ;
   scanf("%d",num);
}
void compute (int num , int *i , int *sum , int *remainder)
{
   *i = num ;

   while (*i != 0)
   {
      *remainder = *i % 10;
      *sum       = *sum + *remainder;
      *i         = *i / 10;
   }
}
void display(int num ,  int sum )
{
   printf("The sum of the digits of the number %d is %d\n",num,sum) ;

}
int main()
{
   int n,j,r,s ;
   input(&n);
   compute(n,&j,&s,&r);
   display(n,s);
   return 0 ;
}