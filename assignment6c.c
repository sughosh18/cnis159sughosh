#include <stdio.h>
#include<math.h>
double series(int n) 
{ 
    int i; 
    double sums = 0.0, ser; 
    for (i = 1; i <= n; ++i) 
    { 
        ser = 1 / pow(i, i); 
        sums += ser; 
    } 
    return sums; 
} 

int main() 
{ 
    int n ;
    printf("Enter the number of terms") ;
    scanf("%d",&n); 
    double res = series(n); 
    printf("The sum of series is %lf\n", res); 
    return 0; 
} 