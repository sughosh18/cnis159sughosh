#include<stdio.h>
void input( int *a , int *b , int *c)
{
	printf("Enter three numbers\n") ;
	scanf("%d%d%d",a,b,c) ;
}
void compute(int a , int b , int c , int *greatest)
{
	if(a>=b && a>=c)
		*greatest = a ;
	else if (b>=a && b>=c)
		*greatest = b ;
	else
		*greatest = c ;
}
void display(int a ,int b ,int c , int greatest)
{
	printf("The largest number is between %d , %d and %d is %d\n",a,b,c,greatest) ;
}
int main()
{
	int x,y,z,largest ;
	input(&x,&y,&z) ;
	compute(x,y,z,&largest) ;
	display(x,y,z,largest) ;
	return 0 ;
}
