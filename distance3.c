#include<stdio.h>
#include<math.h>
struct points
{
    float x1,y1,x2,y2 ;
    float xone , yone , xtwo , ytwo ;
} 
distance ;
void input(float *x1 , float *y1 , float *x2 , float *y2)
{
    printf("Enter the coordinates of the first point\n");
    scanf("%f%f",&distance.x1, &distance.y1) ;
    printf("Enter the coordinates of the second point\n");
    scanf("%f%f",&distance.x2, &distance.y2) ;
}
void compute(float x1 , float y1, float x2 , float y2 , float *final)
{
    *final = sqrt((distance.x2-distance.x1)*(distance.x2-distance.x2)+(distance.y2-distance.y1)*(distance.y2-distance.y1));
}
void display(float x1 , float y1, float x2 , float y2 , float final)
{
    printf("The Points of the first coordinates are %f and %f \n The points of Second coordinate are %f and %f \n The distance between two points is %f\n",distance.x1,distance.y1,distance.x2,distance.y2,final);
}
int main()
{
    float xone , xtwo , yone , ytwo , final ;
    input(&distance.xone,&distance.yone,&distance.xtwo,&distance.ytwo) ;
    compute(xone,yone,xtwo,ytwo,&final) ;
    display(xone,yone,xtwo,ytwo,final);
    return 0 ;
}